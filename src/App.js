import axios from "axios";
import React from "react";
import ZoomVideo from "@zoom/videosdk";
import { ConnectionState } from "@zoom/videosdk";
import { useEffect, useState } from "react";
import "./App.css";

function App() {
  const [sig, setSig] = useState("");
  const [sessionDetail, setSessionDetail] = useState({});
  const client = ZoomVideo.createClient();
  client.on("connection-change", (payload) => {
    if (payload.state === ConnectionState.Connected) {
      // handle connected
      setSessionDetail((curr) => client.getSessionInfo());
      console.log(client.getSessionInfo());
    } else if (payload.state === ConnectionState.Reconnecting) {
      // handle failover
    }
  });
  client.on("video-capturing-change", (payload) => {
    try {
      if (payload.state === "Started") {
        console.log("Capture started");
      } else if (payload.state === "Stopped") {
        console.log("Capture stopped");
      } else {
        console.log("Stop capturing Failed");
      }
    } catch (error) {
      console.log(error);
    }
  });

  let renderedList = [];

  const handleParticipantsChange = (participants) => {
    const stream = client.getMediaStream();
    const canvas = document.getElementById("video-render");
    const videoParticipants = participants.filter((user) => user.bVideoOn);
    const removedVideoParticipants = renderedList.filter(
      (user) =>
        videoParticipants.findIndex((user2) => user2.userId === user.userId) ===
        -1
    );
    if (removedVideoParticipants.length > 0) {
      removedVideoParticipants.forEach(async (user) => {
        await stream.stopRenderVideo(canvas, user.userId);
        // await stream.stopAudio().then(() => console.log("leave audio"));
      });
    }
    const addedVideoParticipants = videoParticipants.filter(
      (user) =>
        renderedList.findIndex((user2) => user2.userId === user.userId) === -1
    );
    if (addedVideoParticipants.length > 0) {
      addedVideoParticipants.forEach(async (user, indx) => {
        // render new video
        console.log(sessionDetail);
        if (user.isHost == true) {
          await stream.renderVideo(canvas, user.userId, 100, 100, 250, 200, 1);
          // await stream.startAudio().then(() => console.log("join audio"));
        } else {
          await stream.renderVideo(canvas, user.userId, 100, 100, 0, 200, 1);
          // await stream.startAudio().then(() => console.log("join audio"));
        }
      });
    }
    renderedList = videoParticipants;
  };

  client.on("user-updated", () => {
    const participants = client.getAllUser();
    console.log("up", participants);
    handleParticipantsChange(participants);
  });
  client.on("user-removed", () => {
    const participants = client.getAllUser();
    console.log("rem", participants);

    handleParticipantsChange(participants);
  });
  useEffect(() => {
    async function getSig() {
      const result = await axios.post(
        "http://103.140.91.46:6514/api/zoom/signature",
        {
          sessionName: "private12",
          role: 1,
          sessionKey: "123456",
          userIdentity: "AgenHadi",
        }
      );
      if (result.data) {
        setSig(result.data.signature);
      }
    }
    getSig();
  }, [sig]);
  async function handlejoin() {
    try {
      await client.init();
      console.log("sukses init");
      if (sig) {
        await client.join("private12", sig, "agent Hadi", "123456");
        console.log("suskses join");
        setTimeout(async () => {
          await client.getMediaStream().startVideo();
          console.log("start capture");
        }, 2000);
        // setTimeout(async () => {
        //   await client.getMediaStream().startAudio();
        //   console.log("join audio");
        // }, 4000);
      } else {
        alert("belum ada signature");
      }
    } catch (error) {
      console.log(error);
    }
  }
  async function handlevideo() {
    try {
      await client.getMediaStream().startVideo();
      console.log("start capture");
    } catch (error) {
      console.log("error capture");
    }
  }
  function handlevideo1() {
    console.log(process.env);
  }
  return (
    <div className="App">
      <h1>Ini halaman percobaan video SDK</h1>
      <p>{sig}</p>
      <button onClick={handlejoin}>join</button>
      <button onClick={handlevideo}>start video</button>
      <button onClick={handlevideo1}>start video1</button>
      <canvas id="video-render" height={400} width={400}></canvas>
    </div>
  );
}

export default App;
